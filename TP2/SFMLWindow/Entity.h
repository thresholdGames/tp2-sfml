#ifndef ENTITY_H
#define ENTITY_H

#include <SFML\Graphics.hpp>
#include <list>

using namespace sf;
using namespace std;

class Entity
{

protected:
	RenderWindow* window;
	Sprite sprite;
	Texture texture;
	Clock clock;
	

public:
	bool active = true;
	static list<Entity> entityList;
	Entity(RenderWindow* _window);
	~Entity();
	virtual void Render();
	virtual void Update() {};
	Sprite getSprite();
};

#endif
