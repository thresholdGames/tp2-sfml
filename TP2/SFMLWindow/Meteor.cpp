#include "Meteor.h"



void Meteor::Render()
{
	if (active)
		window->draw(sprite);
}

void Meteor::Update()
{
	if (!active)
		return;

	sprite.move(direction*speed);

	Time time = clock.getElapsedTime();

	if (time.asSeconds() >= destroyTime)
		active = false;
}

Meteor::Meteor(RenderWindow* _window, Vector2f playerPosition) : Entity(_window)
{
	active = false;
	texture.loadFromFile("cBcodtv.png");
	sprite.setOrigin(texture.getSize().x / 2, texture.getSize().y / 2);
	sprite.setTexture(texture);
	sprite.setScale(2, 2);
}


Meteor::~Meteor()
{
}

Vector2f Meteor::GetRandomSpawn()
{
	Vector2f spawn;
	int random = rand() % 8 + 1;

	switch (random)
	{
		case 1:
			spawn = Vector2f(0, 0);
			break;
		case 2:
			spawn = Vector2f(0, window->getSize().y / 2);
			break;
		case 3:
			spawn = Vector2f(0, window->getSize().y);
			break;
		case 4:
			spawn = Vector2f(window->getSize().x / 2, 0);
			break;
		case 5:
			spawn = Vector2f(window->getSize().x / 2, window->getSize().y);
			break;
		case 6:
			spawn = Vector2f(window->getSize().x , 0);
			break;
		case 7:
			spawn = Vector2f(window->getSize().x, window->getSize().y/2);
			break;
		case 8:
			spawn = Vector2f(window->getSize().x, window->getSize().y);
			break;
		default:
			break;
	}

	return spawn;
}

Vector2f Meteor::Normalize(Vector2f vector)
{
	Vector2f newVector;
	float magnitude = sqrt(vector.x * vector.x + vector.y * vector.y);
	newVector = vector / magnitude;
	return newVector;
}

void Meteor::GetHit()
{
	active = false;
	sprite.setPosition(-1000,-1000);
}

void Meteor::Activate(Vector2f playerPosition)
{
	active = true;
	sprite.setPosition(GetRandomSpawn());
	direction = Normalize(playerPosition - sprite.getPosition());
	clock.restart();
}
