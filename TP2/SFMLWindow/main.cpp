#include "Player.h"
#include "Meteor.h"
#include <iostream>
#include <vector>

#ifdef DEBUG

#include "vld.h"

#endif



using namespace std;

void main()
{
	Text scoreText;
	Text healthText;

	Clock gameClock;
	float spawnTime = 3;
	RenderWindow window(sf::VideoMode(640, 480), "SFML works!");
	Player* player = new Player(&window);
	vector<Meteor*> meteorList = vector<Meteor*>();

	for (int i = 1; i <= 20; i++)
	{
		Meteor* m = new Meteor(&window, player->GetPosition());
		meteorList.push_back(m);
	}


	while (window.isOpen() && player->getHealth()>0)
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		Time time = gameClock.getElapsedTime();

		if (time.asSeconds() >= spawnTime)
		{
			for (int i = 0; i < meteorList.size(); i++)
			{
				if (!meteorList[i]->active)
				{
					meteorList[i]->Activate(player->GetPosition());
					break;
				}
			}
			gameClock.restart();
		}

		window.clear();

		for (int i = 0; i < meteorList.size(); i++)
		{
			if (meteorList[i]->active)
			{
				meteorList[i]->Render();
				meteorList[i]->Update();
			}

			if (meteorList[i]->getSprite().getGlobalBounds().intersects(player->getSprite().getGlobalBounds()))
			{
				player->GetHit();
				meteorList[i]->GetHit();
			}

			/*
			for (list<Bullet*>::iterator  bullet = player->bulletList.begin(); bullet!= player->bulletList.end(); ++bullet)
			{
				if ((*meteor)->getSprite().getGlobalBounds().intersects((*bullet)->getSprite().getGlobalBounds()))
				{
					player->HitMeteor();
					(*meteor)->active = false;
					(*bullet)->active = false;
				}
			}
			
			*/

		}

		player->Render();
		player->Update();

		/*
		string score = ("Score: ") + player->getScore();
		string health = ("Health: ") + player->getHealth();
		scoreText.setString(score);
		scoreText.setString(health);
		*/

		window.display();

		
	}

}