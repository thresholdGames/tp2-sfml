#include "Player.h" 
#include <math.h>


#define M_PI 3.14159265358979323846

Player::Player(RenderWindow* _window):Entity(_window)
{
	texture.loadFromFile("pG0z4k5.png");
	sprite.setOrigin(texture.getSize().x/2, texture.getSize().y / 2);
	sprite.setTexture(texture);
	sprite.setScale(2, 2);
	sprite.setPosition(_window->getSize().x / 2, _window->getSize().y / 2);

	for (int i = 0; i < 20; i++)
	{
		Bullet* bullet = new Bullet(window, GetAngle());
		bulletList.push_back(bullet);
	}

}


Player::~Player()
{
}

void Player::GetHit()
{
	//health--;
}

void Player::HitMeteor()
{
	points += 100;
}

int Player::getScore()
{
	return points;
}

int Player::getHealth()
{
	return health;
}

void Player::Shoot()
{
	Time time = clock.getElapsedTime();

	if (time.asSeconds() <= shootCooldown)
		return;

	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		getFirstBullet()->Activate(sprite.getPosition());
		clock.restart();
	}

}

Bullet* Player::getFirstBullet()
{
	for (int i = 0; i < bulletList.size(); i++)
	{
		if (!bulletList[i]->active)
			return bulletList[i];
	}

	Bullet* bullet = new Bullet(window, GetAngle());
	bulletList.push_back(bullet);
	return bullet;
}

void Player::Move(Event event)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		sprite.move(-speed, 0);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		sprite.move(speed, 0);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		sprite.move(0, -speed);

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		sprite.move(0, speed);
}

void Player::RotateBasedOnMousePosition()
{
	sprite.setRotation(GetAngle());
}

void Player::Update()
{
	Event event;
	window->pollEvent(event);
	Move(event);
	RotateBasedOnMousePosition();
	Shoot();


	for each (Bullet* b in bulletList)
	{
		b->Update();
	}

}

void Player::Render()
{
	window->draw(sprite);
	for each (Bullet* b in bulletList)
	{
		b->Render();
	}
}

Vector2<float> Player::GetPosition()
{
	return sprite.getPosition();
}

float Player::GetAngle()
{
	sf::Mouse mouse;

	float angle = atan2(mouse.getPosition(*window).y - sprite.getPosition().y, mouse.getPosition(*window).x - sprite.getPosition().x);

	angle *= (180 / M_PI);

	if (angle < 0)
	{
		angle = 360 - (-angle);
	}

	return angle;
}


