#include "Entity.h"

class Meteor : public Entity
{
private:
	float speed = 0.01;
	Vector2f direction;
	Vector2f GetRandomSpawn();
	Clock clock;
	float destroyTime = 20;
	Vector2f Normalize(Vector2f source);

public:
	void Render() override;
	void Update() override;
	Meteor(RenderWindow* _window, Vector2f playerPosition);
	void GetHit();
	void Activate(Vector2f playerPosition);
	~Meteor();
};

