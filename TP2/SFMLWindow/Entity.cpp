#include "Entity.h"

list<Entity> Entity::entityList;

Entity::Entity(RenderWindow* _window)
{
	window = _window;
	entityList.push_back(*this);
}


Entity::~Entity()
{
	entityList.push_back(*this);
}

void Entity::Render()
{
	window->draw(sprite);
}

Sprite Entity::getSprite()
{
	return sprite;
}
