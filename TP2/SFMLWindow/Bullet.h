#ifndef BULLET_H
#define BULLET_H

#include "Entity.h"

using namespace sf;

class Bullet : public Entity
{

private:
	float speed = 0.05;
	Vector2f Normalize(Vector2f source);
	float GetAngle();
	Vector2f direction;
	Clock clock;
	float destroyTime = 2;



public:
	Bullet(RenderWindow* _window,float angle);
	~Bullet();

	void GetHit();
	void Activate(Vector2f playerPosition);
	void Update();
	void Render();
};

#endif