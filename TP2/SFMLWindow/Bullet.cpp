#include "Bullet.h"

#define M_PI 3.14159265358979323846


Bullet::Bullet(RenderWindow* _window, float angle) : Entity(_window)
{
	window = _window;
	texture.loadFromFile("ILPYXl9.png");
	sprite.setOrigin(texture.getSize().x / 2, texture.getSize().y / 2);
	sprite.setTexture(texture);
	active = false;
}


Bullet::~Bullet()
{

}

void Bullet::Update()
{
	if (!active)
		return;

	sprite.move(direction*speed);

	Time time = clock.getElapsedTime();

	if (time.asSeconds() >= destroyTime)
		active = false;
}

void Bullet::Render()
{
	if(active)
		window->draw(sprite);
}

Vector2f Bullet::Normalize(Vector2f vector)
{
	Vector2f newVector;
	float magnitude = sqrt(vector.x * vector.x + vector.y * vector.y);
	newVector = vector / magnitude;
	return newVector;
}

float Bullet::GetAngle()
{
	sf::Mouse mouse;

	float angle = atan2(mouse.getPosition(*window).y - sprite.getPosition().y, mouse.getPosition(*window).x - sprite.getPosition().x);

	angle *= (180 / M_PI);

	if (angle < 0)
	{
		angle = 360 - (-angle);
	}

	return angle;
}

void Bullet::GetHit()
{
	active = false;
	sprite.setPosition(1000, 1000);
}

void Bullet::Activate(Vector2f playerPosition)
{
	active = true;
	sprite.setPosition(playerPosition);
	sprite.setRotation(GetAngle());
	direction = Normalize((Vector2f)Mouse::getPosition(*window) - playerPosition);
	clock.restart();
}