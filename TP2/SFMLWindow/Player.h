#ifndef PLAYER_H
#define PLAYER_H


#include <iostream>
#include <SFML\Graphics.hpp>
#include "Entity.h"
#include <vector>
#include "Bullet.h"

using namespace sf;

class Player : public Entity
{

private:
	int points = 0;
	int health = 3;
	float speed = 0.02f;
	void Move(Event event);
	float GetAngle();
	void RotateBasedOnMousePosition();
	void Shoot();
	Clock shoot;
	float shootCooldown = 0.5f;
	Bullet* getFirstBullet();
 
public:
	vector<Bullet*> bulletList;
	void Update() override;
	void Render() override;
	Vector2<float> GetPosition();
	Player(RenderWindow* _window);
	~Player();
	void GetHit();
	void HitMeteor();
	int getScore();
	int getHealth();
};

#endif
